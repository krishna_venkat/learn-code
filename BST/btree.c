#include <stdio.h>
#include "btree.h"

int main()
{
	btree *root = NULL;
	int val, N, idx;

  scanf("%d", &N);

  for (idx = 0; idx < N; idx++) {
    scanf("%d", val);
    root = insert_tree(root, val);
  }
	printf("%d\n", find_height(root));
}
