#ifndef BTREE_H
#define BTREE_H

typedef struct btree
{
	int data;
	struct btree *left, *right;
} btree;

btree *create_node(int data);
btree *insert_tree(btree *root, int data);
int find_height(btree* root);
int find_min(btree *root);
int find_max(btree *root);
void print_in_order(btree *root);
void print_pre_order(btree *root);
void print_post_order(btree *root);
#endif
