#include <stdio.h>
#include <stdlib.h>

typedef struct btree
{
	int data;
	struct btree *left, *right;
} btree;

btree *create_root(int data);
btree *insert_tree(btree *root, int data);
int find_height(btree* root);


btree *create_root(int data)
{
	btree *root = NULL;
	root = malloc(sizeof(btree));
	root->data = data;
	root->left = NULL;
	root->right = NULL;

	return root;
}

int find_height(btree* root) 
{
   if (root==NULL) 
       return 0;
   else
   {
       int left_height = find_height(root->left);
       int right_height = find_height(root->right);
 
       if (left_height > right_height) 
           return(left_height+1);
       else return(right_height+1);
   }
} 

btree *insert_tree(btree *root, int data)
{
	if (root == NULL)
	{
		root = create_root(data);
		return root;
	}

	if (data > root->data)
		root->right = insert_tree(root->right, data);
	else if (data < root->data)
		root->left = insert_tree(root->left, data);

	return root;
}