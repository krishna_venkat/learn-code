#include "gtest/gtest.h"
#include "btree.h"

namespace {

  TEST(test_tree, test_position_node){
      btree* root = NULL;
      insert_tree(root, 8);
      insert_tree(root, 7);
      insert_tree(root, 9);
      insert_tree(root, 10);
      EXPECT_EQ(10, root->right->right->data);
  }

  TEST(test_tree, test_create_tree) {
      btree* root = NULL;
      btree* Root = NULL;
      Root = create_root(root, 5);
      EXPECT_EQ(5, Root->data);
      EXPECT_EQ(NULL, Root->left);
      EXPECT_EQ(NULL, Root->right);
  }

  TEST(test_tree, test_tree_height){
      btree* root = NULL;
      insert_tree(root, 7);
      insert_tree(root, 8);
      insert_tree(root, 9);
      insert_tree(root, 10);
      EXPECT_EQ(4, find_height(root));
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}