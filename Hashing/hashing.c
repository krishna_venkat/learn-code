#include <stdio.h>
#include "hash.h"

int main()
{
	int option, id;
	char name[32], ch;

	hlist_t **hash_table = create_table();
	hlist_t *temp;

	do 
	{
		printf("Enter the option:\n1. Insert\n2. Delete\n3. Find\n4. Delete table\n");
		scanf("%d", &option);

		switch(option)
		{
			case 1:
				printf("Enter name: ");
				scanf("%s", name);
				printf("Enter id: ");
				scanf("%d", &id);
				hash_add(hash_table, name, id);
				break;
			case 2:
				printf("Enter name: ");
				scanf("%s", name);
				hash_delete(hash_table, name);
				break;
			case 3:
				printf("Enter name: ");
				scanf("%s", name);
				temp = hash_search(hash_table, name);
				printf("%s\n", temp->name);
				break;	
		}
		printf("Do you want to continue: ");
		scanf("\n%c", &ch);
	
	}while (ch == 'y');
	return 0;
}
