#include <stdio.h>
#include "hash.h"
#include <stdlib.h>
#include <string.h>

// Create table
hlist_t **create_table(void) 
{
	int index;
	hlist_t **h_hash_tableay = malloc(52 * sizeof(hlist_t *));

	for (index = 0; index < 52; index++) {
		h_hash_tableay[index] = NULL;
	}
	
	return h_hash_tableay;
}

int get_index(char ch)
{
	int index;

	if (ch >= 'A' && ch <= 'Z')
		index = ch - 'A';

	if (ch >= 'a' && ch <= 'z')
		index = ch - 'a' + 26;

	return index;

}

hlist_t *hl_create_node(char *name, int id)
{
	hlist_t *new = malloc(sizeof(hlist_t));

	// Store the name
	strcpy(new->name, name);

	// Store the id
	new -> id = id;

	// Store null in the link
	new -> next = NULL;

	return new;
}

// Insert node at last
void hash_add(hlist_t **hash_table, char *name, int id)
{
	if (hash_table == NULL)
	{
		printf("Error! Create table\n");
		return;
	}
  
	int index = get_index(name[0]);

	// If head is NULL create a node
	if(hash_table[index] == NULL)
	{
		hash_table[index] = hl_create_node(name, id);
	}
	// Else create a node and place it at last
	else
	{
		hlist_t *ptr;
		ptr = hash_table[index];

		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = hl_create_node(name, id);
	}
}


// Delete node 
void hash_delete(hlist_t **hash_table, char *name)
{
	hlist_t *ptr, *prev, *head;

	int index = get_index(name[0]);

	head = hash_table[index];

	// If first node is null
	if (ptr == NULL)
		printf("No list\n");
	
	ptr = head;
	prev = head;
	// Check for the value
	while ((strcmp(ptr->name, name) != 0) && ptr != NULL)
	{
		prev = ptr;
		ptr = ptr->next;
	}

	// If ptr is null
	if (ptr == NULL)
		printf("Name not present\n");

	// If ptr is not the first node
	if (ptr != head)
		prev->next = ptr->next;
	// Else delete the first node
	else
		hash_table[index] = hash_table[index]->next;

	free(ptr);

	ptr = NULL;
	
}

// Find node 
hlist_t *hash_search(hlist_t **hash_table, char *name)
{
	int index = get_index(name[0]);

	hlist_t *ptr;

	// If first node is null
	if (hash_table[index] == NULL)
		printf("No list\n");
	else
	{
		ptr = hash_table[index];

		// Move to next node till value
		while (ptr != NULL && (strcmp(ptr->name, name) != 0))
		{
			ptr = ptr->next;
		}
		
		if (ptr != NULL)
			return ptr;
	}

	// Return the address of the node
		return NULL;
}

void hl_del_list(hlist_t **head)
{
	hlist_t *ptr = *head;

	while (ptr != NULL)
	{
		*head = (*head)->next;
		ptr->next = NULL;
		free(ptr);
		ptr = *head;
	}
}

// Delete list 
hlist_t **hl_del_table(hlist_t **hash_table)
{
	hlist_t *ptr;

	int index;
	
	for (index = 0; index < 52; index++)
	{
		if (hash_table[index] != NULL)
			hl_del_list(&hash_table[index]);
	}
		free(hash_table);
	return NULL;

}


