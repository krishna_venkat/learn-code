#ifndef HASH_H
#define HASH_H

typedef struct hash_list
{
	char name[32];
	int id;
	struct hash_list *next;
}hlist_t;


// Create table
hlist_t **create_table(void);
int get_index(char ch);
hlist_t *hl_create_node(char *name, int id);
void hash_add(hlist_t **arr, char *name, int id);
void hl_del_list(hlist_t **head);
hlist_t **hl_del_table(hlist_t **arr);
void hash_delete(hlist_t **arr, char *name);
hlist_t *hash_search(hlist_t **arr, char *name);
#endif
