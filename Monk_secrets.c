#include <stdio.h>

int front, rear, queue_size = 200000;

void fill_queue_back_with_reduced_powers(int last_index, int max_index, int queue[], int spider_queue[]);
void fill_queue(int queue[], int spider_queue[], int Total_spiders);
void enqueue(int queue[], int queue_size, int *rear, int x);
void dequeue(int queue[], int *front, int *rear); 
int size(int front, int rear);
int Front(int queue[], int front);
int is_empty(int front, int rear);

int main() {
	
	int queue[queue_size], spider_queue[queue_size]; //front = 0, rear = 0;
	int Spiders_to_select, Total_spiders, temp;

	scanf("%d %d", &Total_spiders, &Spiders_to_select);

	fill_queue(queue, spider_queue, Total_spiders);

	temp = Spiders_to_select;

	while (temp--) 
	{

		int last_index, max = -1, max_index;

		if (size(front, rear) > Spiders_to_select) 
		{

			last_index = front + Spiders_to_select;
		} 
		else 
		{
			last_index = rear;
		}

		for (int idx = front; idx < last_index; idx++) {
			int index = queue[idx];

			if (spider_queue[index] > max) {
				max = spider_queue[index];
				max_index = index;
			}
		}

		printf("%d ", max_index + 1);


		fill_queue_back_with_reduced_powers(last_index, max_index, queue, spider_queue);
	}

	return 0;
}

void fill_queue(int queue[], int spider_queue[], int Total_spiders)
{
	for (int idx = 0; idx < Total_spiders; idx++) {
		scanf("%d", &spider_queue[idx]);
		enqueue(queue, queue_size, &rear, idx);
	}

}

void fill_queue_back_with_reduced_powers(int last_index, int max_index, int queue[], int spider_queue[])
{

	for (int idx = front; idx < last_index; idx++) 
	{
			int front_element_index = Front(queue, front);

			dequeue(queue, &front, &rear);

			if (front_element_index != max_index) 
			{

				if (spider_queue[front_element_index]) 
				{
					spider_queue[front_element_index] -= 1;
				}
				enqueue(queue, queue_size, &rear, front_element_index);
			}
	}
}

void enqueue(int queue[], int queue_size, int *rear, int x) 
{
	if (*rear == queue_size) 
	{
		printf("Overflow");
	} else
	{
		queue[*rear] = x;
		(*rear)++;
	}
}

void dequeue(int queue[], int *front, int *rear) 
{
	if (*front == *rear) 
	{
		printf("Underflow");
	}
	else 
	{
		queue[*front] = 0;
		(*front)++;
	}
}

int size(int front, int rear) 
{
	return rear - front;
}

int Front(int queue[], int front) 
{
	return queue[front];
}

int is_empty(int front, int rear) 
{
	return front == rear;
}

