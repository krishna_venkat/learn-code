#include <stdio.h>

#define MAX_PASSES 100000
#define MAX_ID 1000000
#define EVENT_PASS 'P'
#define EVENT_BACK 'B'

int main()
{
	/* declaration of data*/
	unsigned int player_id[MAX_PASSES], current_player, no_of_passes, index = 0;
	unsigned char test_cases, pass_type;

	/* reading number of test cases*/
	scanf("%d", &test_cases);

	/* validating test cases */
	if ( (test_cases < 0) || (test_cases > 100) ) 
		return 1;

	/* looping for all test cases */
	while (test_cases--)
	{
		
		/* reading and validating inputs */
		scanf("%d %d", &no_of_passes, &player_id[index++]);

		if ( (no_of_passes < 0) || (no_of_passes > MAX_PASSES) || (player_id[index - 1] < 0) || (player_id[index - 1] > MAX_ID) )
			return 2;
	
		/* calculating till the last pass */
		while (no_of_passes-- && index++)
		{
			scanf("\n%c", &pass_type);
		
			switch (pass_type)
			{
				case EVENT_PASS:
					scanf("%d", &player_id[index]);
					break;

				case EVENT_BACK:
					player_id[index] = player_id[index - 2];
					break;	
					
				default:
					break;
			}
		}
		
		printf("Player %d\n", player_id[index - 1]);
	}

	return 0;
}
