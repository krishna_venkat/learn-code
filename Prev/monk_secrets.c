#include <stdio.h>
#include "queue.h"
#include "spider_queue.h"

uint_32 big_index;

void fill_queue(QUEUE *spider_queue, uint_32 Total_Spiders);
void find_big_power(QUEUE *Selected_spiders, QUEUE *spider_queue, uint_32 Spiders_To_Select);
void reduce_power(QUEUE *Selected_spiders, uint_32 Spiders_To_Select);
void fill_queue_back(QUEUE *Selected_spiders, QUEUE *spider_queue, uint_32 Spiders_To_Select, uint_32 Total_Spiders);
void select_spiders(QUEUE *Selected_spiders, uint_32 Spiders_To_Select, QUEUE *spider_queue);

int main ()
{
	uint_32 Total_Spiders, Spiders_To_Select;
	uint_16 idx = 0;

	scanf("%d %d", &Total_Spiders, &Spiders_To_Select);

	QUEUE spider_queue[Total_Spiders], Selected_spiders[Spiders_To_Select];

	fill_queue(spider_queue, Total_Spiders);
	print_queue(spider_queue);	

	select_spiders(Selected_spiders, Spiders_To_Select, spider_queue);
	//print_queue(Selected_spiders);	
	
	for (idx = 1; idx <= Spiders_To_Select; idx++)
	{
		find_big_power(Selected_spiders, spider_queue, Spiders_To_Select);	
		printf("Here? %d\n", big_index);
		reduce_power(Selected_spiders, Spiders_To_Select);
		printf("Here again? %d\n", big_index);
		fill_queue_back(Selected_spiders, spider_queue, Spiders_To_Select, Total_Spiders);
	}	
	print_queue(spider_queue);	
}

void fill_queue(QUEUE *spider_queue, uint_32 Total_Spiders)
{
	uint_16 idx = 0;
	QUEUE Spider;

	for (idx = 0 ;idx < Total_Spiders; idx++)
	{
		scanf("%hd", &Spider.power);
		Spider.index = idx + 1;
		en_queue(spider_queue, Total_Spiders, Spider);
	}
}

void select_spiders(QUEUE *Selected_spiders, uint_32 Spiders_To_Select, QUEUE *spider_queue)
{
	uint_16 idx = 0;

	for (idx = 0 ;idx < Spiders_To_Select; idx++)
	{
		Selected_spiders[idx] = de_queue(spider_queue);
		printf("%hd %d\n", Selected_spiders[idx].power, Selected_spiders[idx].index);
	}

}

void find_big_power(QUEUE *Selected_spiders, QUEUE *spider_queue, uint_32 Spiders_To_Select)
{
	uint_16 big_power = 0, idx = 0;
	QUEUE Spider;

	for (idx = 0; idx <= Spiders_To_Select; idx++)
	{
		//printf("%p\n", spider_queue);
		Spider = spider_queue[idx];
		printf("power - %hd\tindex - %d\n", Spider.power, Spider.index);
		//return;	
		if (big_power < Spider.power)
		{
			big_power = Spider.power;
			big_index = idx;
		}
		//en_queue(Selected_spiders, Spiders_To_Select, Spider);
	}
	printf("whaaat? %d\n", Selected_spiders[big_index].index);
}

void reduce_power(QUEUE *Selected_spiders, uint_32 Spiders_To_Select)
{
	uint_16 idx = 0;

	for (idx = 1; idx <= Spiders_To_Select; idx++)
	{
		--(Selected_spiders[idx].power);
	}
}

void fill_queue_back(QUEUE *Selected_spiders, QUEUE *spider_queue, uint_32 Spiders_To_Select, uint_32 Total_Spiders)
{
	uint_16 idx = 0;
	QUEUE Spider;

	for (idx = 0; idx <= Spiders_To_Select; idx++)
	{
		//Spider = de_queue(Selected_spiders);
		//printf("%hd, %d",Spider.power, Spider.index);

		if (Spider.index != big_index)
		{
			en_queue(spider_queue, Total_Spiders, Selected_spiders[idx]);
		}
	}
}
