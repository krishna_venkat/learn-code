    #include<stdio.h>
    #include<string.h>
    #include<stdlib.h>
    #define max(x,y) (x)>(y)?(x):(y)
    #define min(x,y) (x)<(y)?(x):(y)
    int size;
    struct cell{
    	int name;
    	struct cell* next;
    };
    struct game{
    	char gamename[21];
    	int count;
    	struct game* next;
    };
    struct cell **hashname;
    struct game **hashgame;
    void init_hash()
    {
    	int i;
    	for(i=0;i<=size;i++)
    	{
    		hashname[i]=0;
    		hashgame[i]=0;
    	}
    }
     unsigned long
        hash(unsigned char *str)
        {
            unsigned long hash = 5381;
            int c;
     
            while (c = *str++)
                hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
     
            return hash;
        }
    int present(char *str)
    {
    	unsigned long k=hash(str);
    	int pos=k%size;
    	struct cell* cur=hashname[pos];
    	while(cur)
    	{
    		if(cur->name==k)
    		 return 1;
    		cur=cur->next;
    	}
    	struct cell* head=(struct cell*)malloc(sizeof(struct cell));
    	head->name=k;
    	head->next=hashname[pos];
    	hashname[pos]=head;
    	return 0;
    }
    int hash_it(char* str)
    {
    	unsigned long k=hash(str);
    	int pos=k%size;
    	struct game* cur=hashgame[pos];
    	while(cur)
    	{
    		if(strcmp(str,cur->gamename)==0)
    		{
    		 cur->count++;
    		 return cur->count;
    	    }
    		cur=cur->next;
    	}
    	struct game* head=(struct game*)malloc(sizeof(struct game));
    	strcpy(head->gamename,str);
    	head->count=1;
    	head->next=hashgame[pos];
    	hashgame[pos]=head;
    	return 1;
    }
    int main()
    {
    	int n;
    	size=1000000;
    	scanf("%d",&n);
    	hashname=(struct cell**)malloc(size*sizeof(struct cell*));
    	hashgame=(struct game**)malloc(size*sizeof(struct game*));
    	init_hash();
    	char str1[21],str2[21],str3[21];
    	int i,j=0,k,max=0;
    	while(n--)
    	{
    		scanf("%s %s",str1,str2);
    		if(!present(str1))
    		{
    			k=hash_it(str2);
    			if(k>max||(k==max&&(strcmp(str2,str3)<0)))
    			{
    				max=k;
    				strcpy(str3,str2);
    			}
    			if(strcmp(str2,"football")==0)
    			 j++;
    		}
    	}
    	printf("%s\n%d",(strcmp(str3,"badminton")==0?"cricket":str3),j);
    	return 0;
    }