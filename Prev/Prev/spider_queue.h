#ifndef SPIDER_QUEUE
#define SPIDER_QUEUE
#pragma pack(1)

typedef unsigned short int uint_16;
typedef unsigned int uint_32;

typedef struct queue {
	uint_16 power;
	uint_32 index;
} QUEUE;

#endif
