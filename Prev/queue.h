#ifndef QUEUE_H
#define QUEUE_H
#include "spider_queue.h"

int is_full(int size);

int is_empty();

void en_queue(QUEUE *array, int size, QUEUE value);

QUEUE de_queue(QUEUE *array);

void print_queue(QUEUE *array);

#endif
