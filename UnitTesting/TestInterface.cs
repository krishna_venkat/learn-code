﻿
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace Matrix
    {
        interface IAddAlternative
    { 
            int  NumberOFElements(int[] input_array);
            int[,] ReadMatrix(int[] input_array);
            bool CheckMatrixDimention(int[,] matrix);
            int OddPositionSum(int[,] matrix);
            int EvenPositionSum(int[,] matrix);
        }
    }


