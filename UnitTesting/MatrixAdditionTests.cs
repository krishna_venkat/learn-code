﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestClass()]
    public class TestMatrix
    {
        [TestMethod()]
        public void TestNoOFElements()
        {
            //assert
            MatrixAddition matrix = new MatrixAddition();
            int expected = 9;
            int[] input_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //act
            int array_count = matrix.NumberOFElements(input_array);
            //Assert
            Assert.AreEqual(expected, array_count);
        }
        [TestMethod()]
        public void TestMatrixDimention()
        {

            //assert
            MatrixAddition matrix = new MatrixAddition();
            bool expected = true;
            int[,] a = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            //act
            bool Result = matrix.CheckMatrixDimention(a);
            //Assert
            Assert.AreEqual(expected, Result);
        }

        [TestMethod()]
        public void TestEvenSum()
        {
            //Arrange
            MatrixAddition matrix = new MatrixAddition();
            int[] input_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int expected = 20;

            //Act
           int[,]input_matrix= matrix.ReadMatrix(input_array);
            int evenSum = matrix.EvenPositionSum(input_matrix);

            //Assert
            Assert.AreEqual(expected, evenSum);
        }

       

        [TestMethod()]
        public void TestOddSum()
        {
            //Arrange
            MatrixAddition matrix = new MatrixAddition();
            int[] input_array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int expected = 19;

            //Act
            int[,] input_matrix = matrix.ReadMatrix(input_array);
            int oddSum = matrix.OddPositionSum(input_matrix);

            //Assert
            Assert.AreEqual(expected, oddSum);
        }

    }
}