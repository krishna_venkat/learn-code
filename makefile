SRCS := $(wildcard *.c)

TARGET := monk_secret 

${TARGET} : ${SRCS}	
	gcc $^ -o $@

clean : 
	rm ${TARGET}
