#include "gtest/gtest.h"
#include "btree.h"

namespace {

TEST(test_matrix, test_input){
    int matrix[3][3], i, j, k = 0;
    
    for (i = 0; i < 3; i++)
    {    
        for (j = 0; j < 3; j++)
        {
            matrix[i][j] = ++k;
        }
    }
    
    EXPECT_EQ(1, matrix[0][0]);
    EXPECT_EQ(4, matrix[1][0]);
    EXPECT_EQ(7, matrix[2][0]);
    EXPECT_EQ(9, matrix[3][3]);
    
}

TEST(test_matrix, test_odd_element_sum) {
    int matrix[3][3], i, j, k = 0;
    int sum = 0;
    
    for (i = 0; i < 3; i++)
    {    
        for (j = 0; j < 3; j++)
        {
            matrix[i][j] = ++k;
        }
    }
    
    for (i = 0; i < 3; i++)
    {    
        for (j = 0; j < 3; j++)
        {
            if ((i + j)  % 2 != 0 )
            {
                sum += matrix[i][j];
            }
        }
    }
    
    EXPECT_EQ(20, sum);
}

TEST(test_matrix, test_odd_element_sum) {
    int matrix[3][3], i, j, k = 0;
    int sum = 0;
    
    for (i = 0; i < 3; i++)
    {    
        for (j = 0; j < 3; j++)
        {
            matrix[i][j] = ++k;
        }
    }
    
    for (i = 0; i < 3; i++)
    {    
        for (j = 0; j < 3; j++)
        {
            if ((i + j)  % 2 == 0 )
            {
                sum += matrix[i][j];
            }
        }
    }
    
    EXPECT_EQ(25, sum);
}
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}