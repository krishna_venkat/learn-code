// Copyright Notice: (c) 2017 A-dec, Inc.
// All rights reserved

#pragma once

#include <health_and_status.h>
#include <chair_movement_request_signal.h>

//-----------------------------------------------------------------------------
//      ___         __     __   ___  __        __
//     |__  \    / |  \   |  \ |__  /  ` |    /__`
//     |     \/\/  |__/   |__/ |___ \__, |___ .__/
//
//-----------------------------------------------------------------------------
class CppCallback;
class ChairMovementLimitationsSignal;
class ChairPositionSignal;
class SwitchStateSignal;

//-----------------------------------------------------------------------------
//      __             __   __
//     /  ` |     /\  /__` /__`
//     \__, |___ /~~\ .__/ .__/
//
//-----------------------------------------------------------------------------
class ManualMovementRequests final : public HealthAndStatus
{
public:
  ManualMovementRequests();
  ~ManualMovementRequests();

private:

  // Callbacks
  CppCallback                    * const m_callbackChairBackUp;
  CppCallback                    * const m_callbackChairBaseUp;
  CppCallback                    * const m_callbackChairBackDown;
  CppCallback                    * const m_callbackChairBaseDown;
  CppCallback                    * const m_cancelBackUpMovement;
  CppCallback                    * const m_cancelBaseUpMovement;

  // Signals
  ChairMovementLimitationsSignal * const m_p_chairMoveLimitBackSig;
  ChairMovementLimitationsSignal * const m_p_chairMoveLimitBaseSig;
  ChairMovementRequestSignal     * const m_p_chairMoveReqBackSig;
  ChairMovementRequestSignal     * const m_p_chairMoveReqBaseSig;
  SwitchStateSignal              * const m_p_switchStateBackUpSig;
  SwitchStateSignal              * const m_p_switchStateBaseUpSig;
  SwitchStateSignal              * const m_p_switchStateBackDownSig;
  SwitchStateSignal              * const m_p_switchStateBaseDownSig;

  // Functions
  void process_up_movement(SwitchStateSignal const * const              switch_signal_changed,
                           SwitchStateSignal const * const              switch_signal_opposite,
                           ChairMovementRequestSignal           * const chair_movement,
                           ChairMovementLimitationsSignal const * const chair_limits,
                           CppCallback                          * const move_up_callback);

  void process_down_movement(SwitchStateSignal const * const              switch_signal_changed,
                             SwitchStateSignal const * const              switch_signal_opposite,
                             ChairMovementRequestSignal           * const chair_movement,
                             ChairMovementLimitationsSignal const * const chair_limits);

  void chair_back_up_switch_state_changed();
  void chair_base_up_switch_state_changed();
  void chair_back_down_switch_state_changed();
  void chair_base_down_switch_state_changed();
  void cancel_back_up_movement();
  void cancel_base_up_movement();
  bool stop_preset_calibration_chair_movement();
  bool start_movement(SwitchStateSignal const * const              switch_signal_opposite,
                      ChairMovementRequestSignal           * const chair_movement,
                      bool                                         movement_allowed,
                      MovementState                                movement_state,
                      int16_t                                      position_limit);
  bool stop_movement(ChairMovementRequestSignal * const chair_movement,
                     MovementState                      movement_state);

  DISALLOW_COPY_AND_ASSIGN(ManualMovementRequests);
};

