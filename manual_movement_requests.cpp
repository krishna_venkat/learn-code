// Copyright Notice: (c) 2017 A-dec, Inc.
// All rights reserved

#include <manual_movement_requests.h>
#include <framework.h>
#include <callback.h>
#include <chair_movement_limitations_signal.h>
#include <chair_position_signal.h>
#include <switch_state_signal.h>

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------
namespace
{
  constexpr uint16_t LIMP_ALONG_TIME = 1000;
}

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
// CONSTRUCTOR
//=============================================================================
ManualMovementRequests::ManualMovementRequests()
  : m_callbackChairBackUp(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::chair_back_up_switch_state_changed))
  , m_callbackChairBaseUp(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::chair_base_up_switch_state_changed))
  , m_callbackChairBackDown(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::chair_back_down_switch_state_changed))
  , m_callbackChairBaseDown(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::chair_base_down_switch_state_changed))
  , m_cancelBackUpMovement(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::cancel_back_up_movement))
  , m_cancelBaseUpMovement(new CppCallbackWrapper<ManualMovementRequests>(this, &ManualMovementRequests::cancel_base_up_movement))
  , m_p_chairMoveLimitBackSig(Signal::attach<ChairMovementLimitationsSignal>(static_cast<uint8_t>(ChairMovementLimitationsInstance::CHAIR_BACK)))
  , m_p_chairMoveLimitBaseSig(Signal::attach<ChairMovementLimitationsSignal>(static_cast<uint8_t>(ChairMovementLimitationsInstance::CHAIR_BASE)))
  , m_p_chairMoveReqBackSig(Signal::attach<ChairMovementRequestSignal>(static_cast<uint8_t>(ChairMovementRequestInstance::CHAIR_BACK)))
  , m_p_chairMoveReqBaseSig(Signal::attach<ChairMovementRequestSignal>(static_cast<uint8_t>(ChairMovementRequestInstance::CHAIR_BASE)))
  , m_p_switchStateBackUpSig(Signal::attach<SwitchStateSignal>(static_cast<uint8_t>(SwitchStateInstance::CHAIR_BACK_UP)))
  , m_p_switchStateBaseUpSig(Signal::attach<SwitchStateSignal>(static_cast<uint8_t>(SwitchStateInstance::CHAIR_BASE_UP)))
  , m_p_switchStateBackDownSig(Signal::attach<SwitchStateSignal>(static_cast<uint8_t>(SwitchStateInstance::CHAIR_BACK_DOWN)))
  , m_p_switchStateBaseDownSig(Signal::attach<SwitchStateSignal>(static_cast<uint8_t>(SwitchStateInstance::CHAIR_BASE_DOWN)))
{
  VERIFY((&m_callbackChairBackUp != NULL) &&
         (&m_callbackChairBaseUp != NULL) &&
         (&m_callbackChairBackDown != NULL) &&
         (&m_callbackChairBaseDown != NULL) &&
         (&m_cancelBackUpMovement != NULL) &&
         (&m_cancelBaseUpMovement != NULL) &&
         (m_p_chairMoveLimitBackSig != NULL) &&
         (m_p_chairMoveLimitBaseSig != NULL) &&
         (m_p_chairMoveReqBackSig != NULL) &&
         (m_p_chairMoveReqBaseSig != NULL) &&
         (m_p_switchStateBackUpSig != NULL) &&
         (m_p_switchStateBaseUpSig != NULL) &&
         (m_p_switchStateBackDownSig != NULL) &&
         (m_p_switchStateBaseDownSig != NULL));

  // Create callbacks for manual movement
  __CTOR_EXECUTE_IF_HEALTHY_BEG();
  m_p_switchStateBackUpSig->register_signal_attribute_cb(m_callbackChairBackUp, switch_state_signal_members::ACTIVE);
  m_p_switchStateBaseUpSig->register_signal_attribute_cb(m_callbackChairBaseUp, switch_state_signal_members::ACTIVE);
  m_p_switchStateBackDownSig->register_signal_attribute_cb(m_callbackChairBackDown, switch_state_signal_members::ACTIVE);
  m_p_switchStateBaseDownSig->register_signal_attribute_cb(m_callbackChairBaseDown, switch_state_signal_members::ACTIVE);
  __CTOR_EXECUTE_IF_HEALTHY_END();
}

//=============================================================================
// DESTRUCTOR
//=============================================================================
ManualMovementRequests::~ManualMovementRequests()
{
  DELETE_ONLY(m_callbackChairBackUp);
  DELETE_ONLY(m_callbackChairBaseUp);
  DELETE_ONLY(m_callbackChairBackDown);
  DELETE_ONLY(m_callbackChairBaseDown);
  DELETE_ONLY(m_cancelBackUpMovement);
  DELETE_ONLY(m_cancelBaseUpMovement);
}

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//=============================================================================
void ManualMovementRequests::chair_back_up_switch_state_changed ()
{
  __EXECUTE_IF_HEALTHY_BEG()

  process_up_movement(m_p_switchStateBackUpSig, m_p_switchStateBackDownSig,
                      m_p_chairMoveReqBackSig, m_p_chairMoveLimitBackSig, m_cancelBackUpMovement);

  __EXECUTE_IF_HEALTHY_END()
}

//=============================================================================
void ManualMovementRequests::chair_base_up_switch_state_changed ()
{
  __EXECUTE_IF_HEALTHY_BEG()

  process_up_movement(m_p_switchStateBaseUpSig, m_p_switchStateBaseDownSig,
                      m_p_chairMoveReqBaseSig, m_p_chairMoveLimitBaseSig, m_cancelBaseUpMovement);

  __EXECUTE_IF_HEALTHY_END()
}

//=============================================================================
void ManualMovementRequests::chair_back_down_switch_state_changed ()
{
  __EXECUTE_IF_HEALTHY_BEG()

  process_down_movement(m_p_switchStateBackDownSig, m_p_switchStateBackUpSig,
                        m_p_chairMoveReqBackSig, m_p_chairMoveLimitBackSig);

  __EXECUTE_IF_HEALTHY_END()
}

//=============================================================================
void ManualMovementRequests::chair_base_down_switch_state_changed ()
{
  __EXECUTE_IF_HEALTHY_BEG()

  process_down_movement(m_p_switchStateBaseDownSig, m_p_switchStateBaseUpSig,
                        m_p_chairMoveReqBaseSig, m_p_chairMoveLimitBaseSig);

  __EXECUTE_IF_HEALTHY_END()
}

//=============================================================================
// Used for limp-along only
//=============================================================================
void ManualMovementRequests::cancel_back_up_movement ()
{
  if ((m_p_chairMoveReqBackSig->m_movement_state == MovementState::MOVING_UP) &&
      (m_p_chairMoveReqBackSig->m_movement_status == MovementStatus::MANUALLY))
  {
    m_p_chairMoveReqBackSig->stop_movement(UpdateSource::PROGRAMMATIC_UPDATE);
  }
}

//=============================================================================
// Used for limp-along only
//=============================================================================
void ManualMovementRequests::cancel_base_up_movement ()
{
  if ((m_p_chairMoveReqBaseSig->m_movement_state == MovementState::MOVING_UP) &&
      (m_p_chairMoveReqBaseSig->m_movement_status == MovementStatus::MANUALLY))
  {
    m_p_chairMoveReqBaseSig->stop_movement(UpdateSource::PROGRAMMATIC_UPDATE);
  }
}

//=============================================================================
void ManualMovementRequests::process_up_movement (SwitchStateSignal const * const              switch_signal_changed,
                                                  SwitchStateSignal const * const              switch_signal_opposite,
                                                  ChairMovementRequestSignal           * const chair_movement,
                                                  ChairMovementLimitationsSignal const * const chair_limits,
                                                  CppCallback                          * const move_up_callback)
{
  if (TRUE(switch_signal_changed->m_active))
  {
    // Cannot move both the base up and back up at the same time.
    if (TRUE(m_p_switchStateBackUpSig->m_active) && TRUE(m_p_switchStateBaseUpSig->m_active))
    {
      m_p_chairMoveReqBackSig->stop_movement(UpdateSource::FORCED_OVERRIDE);
      m_p_chairMoveReqBaseSig->stop_movement(UpdateSource::FORCED_OVERRIDE);
    }

    // Attempt to start a movement request
    else if (start_movement(switch_signal_opposite,
                            chair_movement,
                            (chair_limits->m_up_movement_allowed && chair_limits->m_upper_pos_limit_valid),
                            MovementState::MOVING_UP,
                            chair_limits->m_upper_pos_limit))
    {
      // Add a delay processing callback for upward movement if limp-along is active
      if (TRUE(chair_limits->m_limp_along_active))
      {
        delay_processing(move_up_callback, LIMP_ALONG_TIME);
      }
    }
    else
    {
      // Nothing to do.
    }
  }
  else
  {
    // Attempt to stop a movement request
    if (stop_movement(chair_movement, MovementState::MOVING_UP))
    {
      // Manual movement was stopped, so cancel any pending limp-along delay processing callbacks
      cancel_delay_processing(move_up_callback);
    }
  }
}

//=============================================================================
void ManualMovementRequests::process_down_movement (SwitchStateSignal const * const              switch_signal_changed,
                                                    SwitchStateSignal const * const              switch_signal_opposite,
                                                    ChairMovementRequestSignal           * const chair_movement,
                                                    ChairMovementLimitationsSignal const * const chair_limits)
{
  if (TRUE(switch_signal_changed->m_active))
  {
    // Attempt to start a movement request
    start_movement(switch_signal_opposite,
                   chair_movement,
                   (chair_limits->m_down_movement_allowed && chair_limits->m_lower_pos_limit_valid),
                   MovementState::MOVING_DOWN,
                   chair_limits->m_lower_pos_limit);
  }
  else
  {
    // Attempt to stop a movement request
    stop_movement(chair_movement, MovementState::MOVING_DOWN);
  }
}

//=============================================================================
bool ManualMovementRequests::stop_movement (ChairMovementRequestSignal * const chair_movement,
                                            MovementState                      movement_state)
{
  bool movement_request_stopped = false;

  // Verify chair is moving in the same direction as the Switch State
  if ((chair_movement->m_movement_state == movement_state) &&
      (chair_movement->m_movement_status == MovementStatus::MANUALLY))
  {
    chair_movement->m_movement_state  = MovementState::STOPPED;
    chair_movement->m_movement_status = MovementStatus::ARRIVED_AT_MANUAL_POSITION;
    chair_movement->publish(UpdateSource::USER_SUPPLIED);
    movement_request_stopped = true;
  }

  return movement_request_stopped;
}

//=============================================================================
bool ManualMovementRequests::start_movement (SwitchStateSignal const * const              switch_signal_opposite,
                                             ChairMovementRequestSignal           * const chair_movement,
                                             bool                                         movement_allowed,
                                             MovementState                                movement_state,
                                             int16_t                                      position_limit)
{
  bool movement_request_started = false;

  // If there is any preposition chair movement stop all movement
  if (FALSE(stop_preset_calibration_chair_movement()))
  {
    // Check if the chair has conflicting directional commands and if so,
    // stop movement for whichever is trying to move (base or back)
    if (TRUE(switch_signal_opposite->m_active))
    {
      chair_movement->stop_movement(UpdateSource::FORCED_OVERRIDE);
    }
    else
    {
      // Make sure there are no other restrictions then move the chair
      if (TRUE(movement_allowed))
      {
        chair_movement->m_target_position = position_limit;
        chair_movement->m_movement_state  = movement_state;
        chair_movement->m_movement_status = MovementStatus::MANUALLY;
        chair_movement->publish(UpdateSource::USER_SUPPLIED);
        movement_request_started = true;
      }
    }
  }

  return movement_request_started;
}

//=============================================================================
bool ManualMovementRequests::stop_preset_calibration_chair_movement ()
{
  bool stop_chair_moving = false;

  bool chair_back_moving = (m_p_chairMoveReqBackSig->m_movement_state != MovementState::STOPPED) &&
                           ((m_p_chairMoveReqBackSig->m_movement_status == MovementStatus::TO_PRESET) ||
                            (m_p_chairMoveReqBackSig->m_movement_status == MovementStatus::CALIBRATION));

  bool chair_base_moving = (m_p_chairMoveReqBaseSig->m_movement_state != MovementState::STOPPED) &&
                           ((m_p_chairMoveReqBaseSig->m_movement_status == MovementStatus::TO_PRESET) ||
                            (m_p_chairMoveReqBaseSig->m_movement_status == MovementStatus::CALIBRATION));

  // Check back and base for preset or calibration movement. If either is moving, stop both
  if (chair_back_moving || chair_base_moving)
  {
    m_p_chairMoveReqBackSig->stop_movement(UpdateSource::FORCED_OVERRIDE);
    m_p_chairMoveReqBaseSig->stop_movement(UpdateSource::FORCED_OVERRIDE);
    stop_chair_moving = true;
  }

  return stop_chair_moving;
}

