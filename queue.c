#include <stdio.h>
#include "queue.h"
#include "spider_queue.h"
#include <stdlib.h>

int front, rear;

int is_full(int size)
{
	if (rear == size)
		return 1;

	else
		return 0;
}

int is_empty()
{
	if (rear == front)
		return 1;

	else
		return 0;
}

void en_queue(QUEUE *array, int size, QUEUE value)
{
	if (is_full(size))
		/*Exiting because the queue can never be full */
		exit(1);

	else
		array[rear++] = value;
}

QUEUE de_queue(QUEUE *array)
{
	if (is_empty())
	{
		exit(1);
	}

	else
	{
		return array[front++];
	}
}

void print_queue(QUEUE *array)
{
	if (is_empty(rear, front))
		printf("Queue is empty\n");

	else
	{
		while (rear != front) {
			printf("%hd\t%d\n", array[front].power, array[front].index);
			front++;
		}
	}
}
